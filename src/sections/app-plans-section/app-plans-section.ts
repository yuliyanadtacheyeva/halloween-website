import './app-plans-section-en.css';
import './app-plans-section-ar.css';

import { AppCard } from '../../components/app-card/app-card.ts';

export function AppPlansSection(): HTMLElement {
  const host = document.createElement('section');
  host.classList.add('plans');
  host.setAttribute('id', 'plans');

  host.innerHTML = `
<div class='container'>
<h2 class='section-heading' data-i18n='plans-heading' id='app-plans-section'></h2>
<div class='cards'></div>
<div class='spider-web'></div>
<div class='skull'></div>
</div>
`;

  const cardsContainer = host.querySelector('.cards');

  cardsContainer?.append(AppCard(1));
  cardsContainer?.append(AppCard(2));
  cardsContainer?.append(AppCard(3));
  return host;
}
