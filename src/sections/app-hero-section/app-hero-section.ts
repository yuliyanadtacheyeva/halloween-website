import './app-hero-section.css';
import calendar from '../../assets/icons/calendar.svg';
import arrowDown from '../../assets/icons/arrow-down.svg';

export function AppHero(): HTMLElement {
  const host = document.createElement('section');
  host.classList.add('hero');
  host.innerHTML = `
   <div class='container'>
     <div class = 'hero-data'>
       <div class="hero-date">
         <img src=${calendar} alt=''>
         <div data-i18n='hero-date'></div>
       </div>
        <h1  class = 'section-heading'  data-i18n='hero-heading'></h1>
       </div>  
     </div>
     <div class="arrow">
       <a href="#plans" class="arrow-link"> <img src=${arrowDown} alt=''></a>
     </div>
    </div> `;
  return host;
}
