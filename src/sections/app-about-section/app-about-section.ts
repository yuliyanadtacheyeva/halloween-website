import './app-about-section-en.css';
import './app-about-section-ar.css';
import { LOCALES } from '../../scripts/locales.ts';
import { StorageService } from '../../scripts/storageService.ts';

import ticket from '../../assets/icons/small-ticket.svg';
import locationIcon from '../../assets/icons/location.svg';
import tickSquare from '../../assets/icons/tick-square.svg';

export function AppAboutSection(): HTMLElement {
  const host = document.createElement('section');
  host.classList.add('about');
  host.setAttribute('id', 'about');

  host.innerHTML = `
<div class='container'>
<h2 class='section-heading' data-i18n='about-heading'></h2>
<h2 class='section-heading' data-i18n='about-subheading'></h2>
<div data-i18n='about-text'></div>

<div class='ticket'>
<div class='ticket-icon'><img src='${ticket}' alt=''></div>
<div class='ticket-info'>
<div data-i18n='about-ticket-date'></div>
<div class = 'about-ticket-place-container'>
<img src='${locationIcon}' alt=''>
<div data-i18n='about-ticket-place'></div>
</div>
</div>
</div>
<div class='checklist'></div>
<div class='spider'></div>
</div>
`;

  function createChecklistItems(locale: keyof typeof LOCALES) {
    const checklistContainer = host.querySelector('.checklist');
    if (!checklistContainer) return;

    Object.keys(LOCALES[locale]).forEach((key) => {
      if (key.startsWith('about-check-')) {
        const outerDiv = document.createElement('div');
        outerDiv.classList.add('checklist-item');

        const iconImg = document.createElement('img');
        iconImg.src = tickSquare;
        iconImg.alt = '';

        const innerDiv = document.createElement('div');
        innerDiv.setAttribute('data-i18n', key);
        innerDiv.textContent = LOCALES[locale][key];

        outerDiv.append(iconImg);
        outerDiv.append(innerDiv);

        checklistContainer.append(outerDiv);
      }
    });
  }
  const locale: keyof typeof LOCALES =
    (StorageService.loadLocale() as keyof typeof LOCALES) || 'en';
  createChecklistItems(locale);

  return host;
}
