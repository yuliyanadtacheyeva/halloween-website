import './app-footer.css';

import icon1 from '../../assets/icons/behance.svg';
import icon2 from '../../assets/icons/Figma.svg';
import icon3 from '../../assets/icons/LinkedIn.svg';
import icon4 from '../../assets/icons/instagram.svg';
import icon5 from '../../assets/icons/youtube.svg';

export function AppFooter(): HTMLElement {
  const host = document.createElement('footer');
  host.classList.add('footer');
  host.setAttribute('id', 'footer');

  const footerBg = document.createElement('div');
  footerBg.classList.add('footer-bg');
  host.appendChild(footerBg);

  const heading = document.createElement('h2');
  heading.classList.add('section-heading');
  heading.setAttribute('data-i18n', 'phone-reservation');
  host.appendChild(heading);

  const socialIconsContainer = document.createElement('div');
  socialIconsContainer.classList.add('social-icons-container');
  host.appendChild(socialIconsContainer);

  const socialIcons = [icon1, icon2, icon3, icon4, icon5];

  socialIcons.forEach((iconSrc, index) => {
    const iconElement = document.createElement('img');
    iconElement.src = iconSrc;
    iconElement.alt = `icon ${index + 1}`;
    iconElement.classList.add('social-icon');
    socialIconsContainer.appendChild(iconElement);
  });

  return host;
}
