import './app-header-section.css';
import { AppButton } from '../../components/app-button/app-button.ts';

export function AppHeader(): HTMLElement {
  const host = document.createElement('header');
  host.classList.add('header');

  host.innerHTML = `
<div class='container'>
  <h1 class = 'party' data-i18n="party-time"></h1>
  <nav>
    <ul>
      <li><a href='#' data-i18n='home'></a></li>
      <li><a href='#gallery' data-i18n="gallery">2</a></li>
      <li><a href='#about' data-i18n="about-party">3</a></li>
      <li><a href='#plans' data-i18n="reservation">4</a></li>
      <li><a href='#footer' data-i18n="contacts">5</a></li>
    </ul>
  </nav>
  <button id='change-locale'>EN عرب</button>
</div>
`;

  const headerContainer = host.querySelector('.container');

  const button = AppButton();
  button.setAttribute('data-i18n', 'header-btn');
  if (headerContainer) {
    headerContainer.append(button);
  }

  return host;
}
