import './app-gallery-section.css';

import image1 from '../../assets/gallery/image1.png';
import image2 from '../../assets/gallery/image2.png';
import image3 from '../../assets/gallery/image3.png';
import image4 from '../../assets/gallery/image4.png';
import image5 from '../../assets/gallery/image5.png';
import image6 from '../../assets/gallery/image6.png';
import image7 from '../../assets/gallery/image7.png';
import image8 from '../../assets/gallery/image8.png';
import image9 from '../../assets/gallery/image9.png';
import image10 from '../../assets/gallery/image10.png';

export function AppGallerySection(): HTMLElement {
  const host = document.createElement('section');
  host.classList.add('gallery');
  host.setAttribute('id', 'gallery');

  const container = document.createElement('div');
  container.classList.add('container');

  const heading = document.createElement('h2');
  heading.classList.add('section-heading');
  heading.setAttribute('data-i18n', 'halloween-memories');
  container.appendChild(heading);

  const galleryContainer = document.createElement('div');
  galleryContainer.classList.add('gallery-container');
  container.appendChild(galleryContainer);

  const images = [
    image1,
    image2,
    image3,
    image4,
    image5,
    image6,
    image7,
    image8,
    image9,
    image10,
  ];

  images.forEach((imageSrc, index) => {
    const imageElement = document.createElement('img');
    imageElement.src = imageSrc;
    imageElement.alt = `Image ${index + 1}`;
    imageElement.classList.add('gallery-image');
    galleryContainer.appendChild(imageElement);
  });

  host.append(container);

  return host;
}
