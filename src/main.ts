import './styles/styles.css';
import { LocaleService } from './scripts/localeService.ts';
import { StorageService } from './scripts/storageService.ts';

import { AppHeader } from './sections/app-header-section/app-header-section.ts';
import { AppHero } from './sections/app-hero-section/app-hero-section.ts';
import { AppGallerySection } from './sections/gallery/app-gallery-section.ts';
import { AppAboutSection } from './sections/app-about-section/app-about-section.ts';
import { AppPlansSection } from './sections/app-plans-section/app-plans-section.ts';
import { AppFooter } from './sections/app-footer/app-footer.ts';


document.addEventListener('DOMContentLoaded', () => {
  const localeService = new LocaleService();

  localeService.fetchLocalesFromBackend().then((fetchedLocales) => {
    localeService.locales = fetchedLocales;

    localeService.currentLocale = StorageService.loadLocale() || 'en';
    localeService.updateDirection();
    localeService.translateAllTextOnPage();
  });

  document.querySelector('#change-locale')?.addEventListener('click', () => {
    localeService.changeLocale();
  });
});

const app = document.querySelector('#app');

const header = AppHeader();
const hero = AppHero();
const gallery = AppGallerySection();
const about = AppAboutSection();
const plans = AppPlansSection();
const footer = AppFooter();

app?.append(header);
app?.append(hero);
app?.append(gallery);
app?.append(about);
app?.append(plans);
app?.append(footer);
