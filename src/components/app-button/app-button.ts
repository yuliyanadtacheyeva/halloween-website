import './app-button.css';

export function AppButton(): HTMLButtonElement {
  const host = document.createElement('button');
  host.classList.add('app-button');
  return host;
}
