import './app-card-en.css';
import './app-card-ar.css';
import { AppButton } from '../app-button/app-button.ts';

let isKeyboardFocus = false;
function handleKeydownEvent(e: KeyboardEvent): void {
  if (e.key === 'Tab') {
    isKeyboardFocus = true;
  }
}

function handleMousedownEvent(): void {
  isKeyboardFocus = false;
}

document.addEventListener('keydown', handleKeydownEvent);
document.addEventListener('mousedown', handleMousedownEvent);

export function AppCard(cardNumber: number): HTMLElement {
  const host = document.createElement('div');
  host.className = `card card-${cardNumber}`;
  host.tabIndex = 0;

  host.addEventListener(
    'focus',
    () => {
      if (isKeyboardFocus) {
        host.classList.add('keyboard-focus');
      }
    },
    true,
  );

  host.addEventListener(
    'blur',
    () => {
      host.classList.remove('keyboard-focus');
    },
    true,
  );

  const title = document.createElement('h2');
  title.setAttribute('data-i18n', `${cardNumber}-card-title`);
  title.tabIndex = 0;
  host.append(title);

  const price = document.createElement('p');
  price.className = 'price';
  price.setAttribute('data-i18n', `${cardNumber}-card-price`);
  price.tabIndex = 0;
  host.append(price);

  const features = document.createElement('ul');
  for (let i = 1; i <= 3; i++) {
    const feature = document.createElement('li');
    feature.setAttribute('data-i18n', `${cardNumber}-card-feature-${i}`);
    feature.tabIndex = 0;
    features.append(feature);
  }
  host.append(features);

  const button = AppButton();
  button.setAttribute('data-i18n', 'card-btn');
  host.append(button);

  return host;
}
