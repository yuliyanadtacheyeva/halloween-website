export class StorageService {
  static saveLocale(locale: string) {
    localStorage.setItem('userLocale', locale);
  }

  static loadLocale(): string | null {
    return localStorage.getItem('userLocale');
  }
}
