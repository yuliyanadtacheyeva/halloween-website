export type LocaleKey = 'en' | 'ar';

export type LocaleMessages = {
  [key: string]: string;
};

export type Locales = {
  [key in LocaleKey]: LocaleMessages;
};

export const LOCALES: Locales = {
  en: {
    'party-time': 'Party Time!',
    home: 'Home',
    gallery: 'Gallery',
    'about-party': 'About Party',
    reservation: 'Reservation',
    contacts: 'Contacts',
    'header-btn': 'Reservation',

    //---

    'hero-date': '31 OCTOBER 2023',
    'hero-heading': "IT'S HALLOWEEN PARTY O'CLOCK!",

    //---

    'halloween-memories': 'HALLOWEEN MEMORIES',

    //---

    'about-heading': 'JOIN US',
    'about-subheading': "THIS YEAR'S HALLOWEEN PARTY!",
    'about-text':
      'OUR HALLOWEEN PARTY THIS YEAR WILL BE AN UNFORGETTABLE EXPERIENCE, FILLED WITH SPOOKY DECORATIONS, EERIE MUSIC, THRILLING GAMES, AND COSTUME CONTESTS. JOIN US FOR A NIGHT OF MAGIC AND FRIGHT!',
    'about-ticket-date': 'Tue, 31 October 2023, 19:00',
    'about-ticket-place': 'The Menza Club, Istanbul city',
    'about-check-1': 'NightWitch Hunt',
    'about-check-2': 'Bestcostume Contest',
    'about-check-3': 'DeliciousCakes And Sweets',
    'about-check-4': 'Everybody Get Tipsy',
    'about-check-5': 'Dance-Off With A Star Guest',

    //---

    'plans-heading': "LET'S BE YOUR HOSTS",
    '1-card-title': 'Single Ticket',
    '1-card-price': '$100',
    '1-card-feature-1': 'Blood Drink',
    '1-card-feature-2': 'Haunted House Tour',
    '1-card-feature-3': 'Horror Movie Marathon',

    '2-card-title': 'Couple Ticket',
    '2-card-price': '$150',
    '2-card-feature-1': 'Blood Drink',
    '2-card-feature-2': 'Horror Movie Marathon',
    '2-card-feature-3': 'Pumpkin Carving Contest',

    '3-card-title': 'Combine Ticket',
    '3-card-price': '$300',
    '3-card-feature-1': 'Blood Drink',
    '3-card-feature-2': 'Haunted House Tour',
    '3-card-feature-3': 'Horror Movie Marathon',

    'card-btn': 'Buy Ticket',

    //---
    'phone-reservation': 'PHONE RESERVATION? (+1) 987 46 52',
  },

  ar: {
    'party-time': 'پارتی تایم',
    home: 'خانه',
    gallery: 'گالری',
    'about-party': 'درباره پارتی',
    reservation: 'رزرو میز',
    contacts: 'تماس با ما',

    'header-btn': 'رزرو میز',
    //---

    'hero-date': '31 اکتبر 2023',
    'hero-heading': '!اینجا هالوین پارتی به راهه',

    //---

    'halloween-memories': 'هالوین پارتی های قبلیمون',

    //---

    'about-heading': 'میاید دیگه؟',
    'about-subheading': ' هالوین پارتی امسال همگی دعوتید',
    'about-text':
      'جشن هالووین امسال ما، تجربه‌ای فراموش‌نشدنی خواهد بود؛ پر از تزئینات ترسناک، موسیقی ترسناک، بازی‌های هیجان‌انگیز و مسابقات لباس. به ما بپیوندید برای یک شب پر از جادو و ترس',
    'about-ticket-date': 'دوشنبه، 31  اکتبر 2023، ساعت 19:00',
    'about-ticket-place': 'کلاب منزا، شهر استانبول',
    'about-check-1': 'شب جادو و شکار',
    'about-check-2': 'مسابقه بهترین کاستوم',
    'about-check-3': 'کیک و شیرینیای خوشمزه',
    'about-check-4': 'انواع شراب و الکلیجات',
    'about-check-5': 'رقص با مهمون ویژه',

    //---

    'plans-heading': 'میزبان شما هستیم',

    '1-card-title': 'بلیت تک نفره',
    '1-card-price': '$100',
    '1-card-feature-1': 'معجون خون',
    '1-card-feature-2': 'شب وحشی ترسناک',
    '1-card-feature-3': 'اتاق وحشت',

    '2-card-title': 'بلیت زوج نفره',
    '2-card-price': '$150',
    '2-card-feature-1': 'معجون خون',
    '2-card-feature-2': 'شب وحشی ترسناک',
    '2-card-feature-3': 'اتاق وحشت',

    '3-card-title': 'بلیت گروهی',
    '3-card-price': '$300',
    '3-card-feature-1': 'معجون خون',
    '3-card-feature-2': 'شب وحشی ترسناک',
    '3-card-feature-3': 'اتاق وحشت',

    'card-btn': 'خرید بلیت',

    //---

    'phone-reservation': 'رزرو تلفنی؟  987 46 52  (+1)',
  },
};
