import { Locales, LOCALES } from './locales.ts';
import { StorageService } from './storageService.ts';

export class LocaleService {
  currentLocale: string;
  locales: Locales | null;

  constructor() {
    this.currentLocale = 'en';
    this.locales = null;
  }

  changeLocale() {
    this.currentLocale = this.currentLocale === 'en' ? 'ar' : 'en';
    StorageService.saveLocale(this.currentLocale);
    this.updateDirection();
    this.translateAllTextOnPage();
  }

  updateDirection() {
    const body = document.body;
    body.dir = this.currentLocale === 'en' ? 'ltr' : 'rtl';

    const htmlElement = document.querySelector('html');
    htmlElement?.setAttribute('lang', this.currentLocale);
  }

  translateAllTextOnPage() {
    if (!this.locales) {
      console.error('Locales data is not loaded.');
      return;
    }
    const localeMessages = this.locales[this.currentLocale as keyof Locales];
    const elementsForTranslation =
      document.querySelectorAll<HTMLElement>('[data-i18n]');

    elementsForTranslation.forEach((element) => {
      const key = element.getAttribute('data-i18n');
      if (key && localeMessages[key]) {
        element.innerHTML = localeMessages[key];
      }
    });
  }

  fetchLocalesFromBackend(): Promise<Locales> {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(LOCALES);
      }, 100);
    });
  }
}
